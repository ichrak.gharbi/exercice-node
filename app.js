const express = require('express');
const {check, validationResult} = require('express-validator');
const app = express();

app.use(express.json());

let articles = [
    {

        id:1,
        title: "First post",
        content: "lorem ipsum"
    },
    {

        id:2,
        title: "Second post",
        content: "lorem ipsum"
    },
    {

        id:3,
        title: "Third post",
        content: "lorem ipsum"
    }
]


// get 

app.get('/', (req,res) => {
 
    res.json('hello world');

});


app.get('/api/articles', (req,res) => {
 
    res.json(articles);

});



// post


app.post('/api/articles', (req,res) => {
    

    if (!req.body.title) {

        return res.status(400).json({
            status: 400,
            message: "title is required"
        })
    }


const article = {
    id: Math.floor((Math.random()*100)),
    title: req.body.title,
    content: req.body.content
}

articles.push(article);
res.json(article);
});

//put

app.put('/api/articles/:id',
[
check('title').notEmpty(),
check('content').isLength({min: 5})

], (req, res) => {

    
    let id = req.params.id;
    const article = articles.find(a => a.id == id);


    if(!article) {

        return res.status(404).json({

            status: 404,
            message:"Article not found"
        })

    }

    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(400).json({
            status: 400,
            errors:errors.array()
        })
    }



    article.title = req.body.title;
    article.content = req.body.content;

res.json(article);

});
      
//delete
app.delete('/api/articles/:id', (req, res) =>{

const id = req.params.id;
const article = articles.find(a => a.id == id);


if(!article) {
    return res.status(404).json({
        status: 404,
        message:"Article not found"
    })
}

const index = articles.indexOf(article);
articles.splice(index, 1);
res.json({
    message: "article deledet successfully"
});

})




app.listen(3000, () => {
console.log("listening on port 3000");
});